var mysql      = require('mysql');
var request = require("request");
var cheerio = require("cheerio");
var async = require("async");
var connection = mysql.createConnection({
host     : 'localhost',
user     : 'root',
password : 'root',
database : 'justin.unionfacts.com'
});

connection.connect();
var query = "select * from nlrb_dl_node left join nlrb_scrape_c on nlrb_dl_node.`case number` = nlrb_scrape_c.case_number where case_number is null and `case number` like \"%-C%\" limit 1000;";

connection.query(query, null, function (error, results, fields) {
  async.eachSeries(results, function(r, top_callback) {
var url = "https://www.nlrb.gov/case/"+r["Case Number"];
    request(url, function (error, response, body) {
      if (!error) {
        var $ = cheerio.load(body);
        console.log("request"+url);
        var case_meta = {
          "case_number":$(".views-field-case .field-content").text(),
          "city":$(".views-field-city .field-content").text(),
          "date_filed":$(".views-field-date-filed .field-content").text(),
          "status":$(".views-field-status .field-content").text(),
          "close_method": $(".views-field-close-method .field-content").text()
        };
        //console.log(case_meta);

        var docket = [];
        var docket_array = [];
        $('.view-docket-activity table tr').not('thead tr').each(function(){
          var docket_item = {
            "case_number":$(".views-field-case .field-content").text(),
            "date":$(this).find(".views-field-ds-activity").text().trim(),
            "document":$(this).find(".views-field-da-doctype").text().trim(),
            "issue-filed":$(this).find(".views-field-participant-type").text().trim(),
          }
          var docket_array_item = [$(".views-field-case .field-content").text(),
                                    $(this).find(".views-field-ds-activity").text().trim(),
                                    $(this).find(".views-field-da-doctype").text().trim(),
                                    $(this).find(".views-field-participant-type").text().trim()
                                  ];
            docket.push(docket_item);
            docket_array.push(docket_array_item);
        });
        console.log(docket_array);


        var participants = [];
        var p_array = [];
        $(".view-participants table tr").not('thead tr').each(function(){
          var participant = {
            "case_number":$(".views-field-case .field-content").text(),
            "participant_role":$(this).find(".views-field-participant-type div").first().text(),
            "union-employer":$(this).find(".views-field-participant-type div:nth-child(2)").text(),
            "name":$(this).find(".views-field-participant-type div:nth-child(3)").text(),
            "association":$(this).find(".views-field-participant-type div:nth-child(4)").text(),
            "address": $(this).find(".views-field-zip").text(),
            "phone": $(this).find(".views-field-phone").text().trim()
          };
          p_array_item = [$(".views-field-case .field-content").text(),
          $(this).find(".views-field-participant-type div").first().text(),
          $(this).find(".views-field-participant-type div:nth-child(2)").text(),
          $(this).find(".views-field-participant-type div:nth-child(3)").text(),
          $(this).find(".views-field-participant-type div:nth-child(4)").text(),
          $(this).find(".views-field-zip").text(),
          $(this).find(".views-field-phone").text().trim()
        ]
          participants.push(participant);
          p_array.push(p_array_item);
        });

        var allegations = [];
        $(".view-allegations ul li").each(function(){
          allegations.push([$(".views-field-case .field-content").text(),$(this).find("div span").text()]);
          //allegations.push();
        });

        //callback();
        if(case_meta.case_number){
          console.log(case_meta);
          async.parallel([
            function(callback){
              connection.query('INSERT INTO nlrb_scrape_c SET ?', case_meta, function(err, result) {
              if(err)throw err;
              console.log("Inserted Case Meta")
              callback();
            });
            },
            function(callback){
              if(docket_array.length != 0){
                //console.log("Docket Array: "+docket_array)
                connection.query('INSERT INTO nlrb_scrape_c_docket  (case_number, date,document,`issue-filed`) VALUES ?', [docket_array], function(err, result) {
                if(err)throw err;
                console.log("Inserted Docket")
                callback();
              });
              }else{
                console.log("No Docket")
                callback();
              }
            },
            function(callback){
              if(p_array.length != 0){
                //console.log("Participant Array: "+p_array)
                connection.query('INSERT INTO nlrb_scrape_c_participants (case_number,participant_role,`union-employer`,name,association,address,phone) VALUES ?', [p_array], function(err, result) {
                if(err)throw err;
                console.log("Inserted Participants")
                callback();
                });
              }else{console.log("No Participants");
              callback();}
            },
            function(callback){
              if(allegations.length != 0){
                connection.query('INSERT INTO nlrb_scrape_c_allegations (case_number,allegation) VALUES ?', [allegations], function(err, result) {
                if(err)throw err;
                console.log("Inserted Allegations")
                callback();
              });
            }else{console.log("No Allegation s");callback();}
            },

          ],
            function(err, results) {
            if (err) return next(err); //If an error occurred, we let express handle it by calling the `next` function
            top_callback();
          });


        //
        // connection.query('INSERT INTO nlrb_scrape_c SET ?', case_meta, function(err, result) {
        //     console.log("docket"+docket_array)
        //   connection.query('INSERT INTO nlrb_scrape_c_docket  (case_number, date,document,`issue-filed`) VALUES ?', [docket_array], function(err, result) {
        //     console.log(p_array)
        //     connection.query('INSERT INTO nlrb_scrape_c_participants (case_number,participant_role,`union-employer`,name,association,address,phone) VALUES ?', [p_array], function(err, result) {
        //       console.log(allegations);
        //       connection.query('INSERT INTO nlrb_scrape_c_allegations (case_number,allegation) VALUES ?', [allegations], function(err, result) {callback();
        //
        //         if(err)throw err;
        //           console.log("inserted allegations");
        //
        //       });
        //       if(err)throw err;
        //         console.log("inserted participant");
        //
        //     });
        //     if(err)throw err;
        //       console.log("inserted docket");
        //
        //   });
        //   if(err)throw err;
        //     console.log("inserted");
        //
        // });

      }

      }else{
        console.log(error);
      }
  });


  }, function(err) {
      // if any of the file processing produced an error, err would equal that error
      if( err ) {
        // One of the iterations produced an error.
        // All processing will now stop.
        console.log('A file failed to process');
      } else {
        console.log('All files have been processed successfully');
      }
  });

});
