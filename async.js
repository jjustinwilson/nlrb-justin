var async = require("async");

var array = [1,2,3];
async.each(array, function(r, callback) {

  console.log(r);
  callback();

}, function(err) {
    // if any of the file processing produced an error, err would equal that error
    if( err ) {
      // One of the iterations produced an error.
      // All processing will now stop.
      console.log('A file failed to process');
    } else {
      console.log('All files have been processed successfully');
    }
});
