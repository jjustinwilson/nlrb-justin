var request = require("request"),
  cheerio = require("cheerio"),
  url = "https://www.nlrb.gov/case/18-CB-126869";
  console = require('better-console');
  var mysql      = require('mysql');
  var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'justin.unionfacts.com'
});

request(url, function (error, response, body) {
  if (!error) {
    var $ = cheerio.load(body),
      case_number = $(".views-field-case .field-content").text(),
      city = $(".views-field-city .field-content").text(),
      date_filed = $(".views-field-date-filed .field-content").text(),
      status = $(".views-field-status .field-content").text(),
      close_method = $(".views-field-close-method .field-content").text();

      var allegations = [];
      $(".view-allegations ul li").each(function(){
        allegations.push("item"+$(this).find("div span").text());
        //allegations.push();
      });

      var participants = [];

      $(".view-participants table tr").not('thead tr').each(function(){
        var participant = {
          "participant_role":$(this).find(".views-field-participant-type div").first().text(),
          "union-employer":$(this).find(".views-field-participant-type div:nth-child(2)").text(),
          "name":$(this).find(".views-field-participant-type div:nth-child(3)").text(),
          "association":$(this).find(".views-field-participant-type div:nth-child(4)").text(),
          "address": $(this).find(".views-field-zip").text(),
          "phone": $(this).find(".views-field-phone").text().trim()
        };
        participants.push(participant);
      });

      //console.log(participants)

      var docket = [];
      $('.view-docket-activity table tr').not('thead tr').each(function(){
        var docket_item = {
          "date":$(this).find(".views-field-ds-activity").text().trim(),
          "document":$(this).find(".views-field-da-doctype").text().trim(),
          "issue-filed":$(this).find(".views-field-participant-type").text().trim(),
        }
          docket.push(docket_item);
      });

    //console.log(city+date_filed+status+close_method);
    console.log(docket);
  } else {
    console.log("We’ve encountered an error: " + error);
  }
});
