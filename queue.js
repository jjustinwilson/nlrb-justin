var mysql      = require('mysql');
var request = require("request");
var cheerio = require("cheerio");
var async = require("async");
require('http').globalAgent.maxSockets = Infinity

var connection = mysql.createConnection({
host     : 'localhost',
user     : 'root',
password : 'root',
database : 'justin.unionfacts.com'
});
var inserted = 0;
var start_time = Date.now();
var q = async.queue(function(task, q_callback) {
            var url = "https://www.nlrb.gov/case/"+task.name;
            console.log("Making Request of "+url);
            request(url, function (error, response, body) {
              if (!error) {
                //console.log("Status Code: "+response.statusCode)
                response.on("end", function() {
                  console.log("End Status Code: " + statcode);
                });
                var $ = cheerio.load(body);
                //console.log("request"+url);
                var case_meta = {
                  "case_number":$(".views-field-case .field-content").text(),
                  "city":$(".views-field-city .field-content").text(),
                  "date_filed":$(".views-field-date-filed .field-content").text(),
                  "status":$(".views-field-status .field-content").text(),
                  "close_method": $(".views-field-close-method .field-content").text()
                };
                //console.log(case_meta);

                var docket = [];
                var docket_array = [];
                $('.view-docket-activity table tr').not('thead tr').each(function(){
                  var docket_item = {
                    "case_number":$(".views-field-case .field-content").text(),
                    "date":$(this).find(".views-field-ds-activity").text().trim(),
                    "document":$(this).find(".views-field-da-doctype").text().trim(),
                    "issue-filed":$(this).find(".views-field-participant-type").text().trim(),
                  }
                  var docket_array_item = [$(".views-field-case .field-content").text(),
                                            $(this).find(".views-field-ds-activity").text().trim(),
                                            $(this).find(".views-field-da-doctype").text().trim(),
                                            $(this).find(".views-field-participant-type").text().trim()
                                          ];
                    docket.push(docket_item);
                    docket_array.push(docket_array_item);
                });
                //console.log(docket_array);


                var participants = [];
                var p_array = [];
                $(".view-participants table tr").not('thead tr').each(function(){
                  var participant = {
                    "case_number":$(".views-field-case .field-content").text(),
                    "participant_role":$(this).find(".views-field-participant-type div").first().text(),
                    "union-employer":$(this).find(".views-field-participant-type div:nth-child(2)").text(),
                    "name":$(this).find(".views-field-participant-type div:nth-child(3)").text(),
                    "association":$(this).find(".views-field-participant-type div:nth-child(4)").text(),
                    "address": $(this).find(".views-field-zip").text(),
                    "phone": $(this).find(".views-field-phone").text().trim()
                  };
                  p_array_item = [$(".views-field-case .field-content").text(),
                  $(this).find(".views-field-participant-type div").first().text(),
                  $(this).find(".views-field-participant-type div:nth-child(2)").text(),
                  $(this).find(".views-field-participant-type div:nth-child(3)").text(),
                  $(this).find(".views-field-participant-type div:nth-child(4)").text(),
                  $(this).find(".views-field-zip").text(),
                  $(this).find(".views-field-phone").text().trim()
                ]
                  participants.push(participant);
                  p_array.push(p_array_item);
                });

                var allegations = [];
                $(".view-allegations ul li").each(function(){
                  allegations.push([$(".views-field-case .field-content").text(),$(this).find("div span").text()]);
                  //allegations.push();
                });

                //callback();
                if(case_meta.case_number){
                  //console.log(case_meta);
                  async.parallel([
                    function(callback){
                      connection.query('INSERT INTO nlrb_scrape_c SET ?', case_meta, function(err, result) {
                      if(err)throw err;
                      //console.log("Inserted Case Meta")
                      callback();
                    });
                    },
                    function(callback){
                      if(docket_array.length != 0){
                        //console.log("Docket Array: "+docket_array)
                        connection.query('INSERT INTO nlrb_scrape_c_docket  (case_number, date,document,`issue-filed`) VALUES ?', [docket_array], function(err, result) {
                        if(err)throw err;
                        //console.log("Inserted Docket")
                        callback();
                      });
                      }else{
                        //console.log("No Docket")
                        callback();
                      }
                    },
                    function(callback){
                      if(p_array.length != 0){
                        //console.log("Participant Array: "+p_array)
                        connection.query('INSERT INTO nlrb_scrape_c_participants (case_number,participant_role,`union-employer`,name,association,address,phone) VALUES ?', [p_array], function(err, result) {
                        if(err)throw err;
                        //console.log("Inserted Participants")
                        callback();
                        });
                      }else{console.log("No Participants");
                      callback();}
                    },
                    function(callback){
                      if(allegations.length != 0){
                        connection.query('INSERT INTO nlrb_scrape_c_allegations (case_number,allegation) VALUES ?', [allegations], function(err, result) {
                        if(err)throw err;
                        //console.log("Inserted Allegations")
                        callback();
                      });
                    }else{console.log("No Allegation s");callback();}
                    },

                  ],
                    function(err, results) {
                    if (err) return next(err); //If an error occurred, we let express handle it by calling the `next` function
                    console.log("Next Item Called")
                    q_callback();
                    inserted++;
                    console.log("Inserted: "+inserted)
                    var insert_time = Date.now();
                    var time_diff = (insert_time - start_time)/1000;
                    if(time_diff > 10){console.log("Insertions Per Minute: "+inserted/(time_diff/60))}
                  });
              }
              }else{
                console.log("THere's been an error"+error);

                q_callback();
              }
          }).on('error', function(e){
              console.log(e)

          }).end();
}, 9);
q.drain = function() {
    console.log('all items have been processed');
    connection.end();
    process.exit();
};

connection.connect();
var query = "select distinct `Case Number` from nlrb_dl_node left join nlrb_scrape_c on nlrb_dl_node.`case number` = nlrb_scrape_c.case_number where case_number is null and `case number` like \"%-C%\" limit 50000;";
query = "select distinct `Case Number` from nlrb_dl_node  left join nlrb_scrape_c on nlrb_dl_node.`case number` = nlrb_scrape_c.case_number where case_number is null and `case number` like \"%-C%\" limit 50000;"
connection.query(query, null, function (error, results, fields) {
    for(i in results){
        //console.log(results[i]["Case Number"])
      q.push({name: results[i]["Case Number"]}, function(err) {
            //console.log('Processing '+results[i]["Case Number"]);
          });
    }
});
