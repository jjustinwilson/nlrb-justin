var request = require("request");
var csv = require('csv');
var async = require("async");
var mysql      = require('mysql');
var connection = mysql.createConnection({
host     : 'localhost',
user     : 'root',
password : 'root',
database : 'justin.unionfacts.com'
});
connection.connect();
//https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/xml?f[0]=s%3AOpen%20-%20Blocked&f[1]=date%3A04/01/2016%20to%2009/01/2016
//https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/cvs?f[0]=s%3AOpen%20-%20Blocked&f[1]=date%3A04/01/2016%20to%2009/01/2016&attach=page
//All https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/cvs?sort_by=date_filed&sort_order=DESC&items_per_page=60&attach=page

//October   https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/cvs?f[0]=date%3A10/01/2016%20to%2010/31/2016&attach=page

//var url = "https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/cvs?f[0]=s%3AOpen%20-%20Blocked&f[1]=date%3A04/01/2016%20to%2009/01/2016&attach=page"


function getCSV(){
  request(url, function (error, response, body) {
    if (!error) {
      //console.log(response);
      console.log("body")
      csv.parse(body, function(err, data){

          //console.log(data);
          connection.query('INSERT INTO nlrb_dl_node (Name,`Case Number`,City,State,`Date Filed`, `Region Assigned`,Status,`Date Closed`,`Reason Closed`,`No. of Eligible Voters`,`No. of Employees`,`Certified Representative`,`Unit Sought`) VALUES ?', [data], function(err, result) {
            if(err)throw err;
              console.log("bulk insert");
              connection.end();
          });
      });
    } else {
      console.log("We’ve encountered an error: " + error);
    }
  });
}
//Jan 1, 2005 = 1104537600
//
// var date = new Date(unix_timestamp*1000), date_end = new Date((unix_timestamp+604800)*1000)
// console.log(date_end)
// var start_date = ("0" + (date.getMonth() + 1)).slice(-2)+"/"+("0" + date.getDate()).slice(-2)+"/"+date.getFullYear();
// var end_date = ("0" + (date_end.getMonth() + 1)).slice(-2)+"/"+("0" + date_end.getDate()).slice(-2)+"/"+date_end.getFullYear();
// console.log(start_date+end_date);


//Loop
var now = 1477855004
var unix_timestamp = 1464986859
async.whilst(function () {
  return unix_timestamp <= now;
},
function (next) {
  var date = new Date(unix_timestamp*1000), date_end = new Date((unix_timestamp+604800)*1000)
  var start_date = ("0" + (date.getMonth() + 1)).slice(-2)+"/"+("0" + date.getDate()).slice(-2)+"/"+date.getFullYear();
  var end_date = ("0" + (date_end.getMonth() + 1)).slice(-2)+"/"+("0" + date_end.getDate()).slice(-2)+"/"+date_end.getFullYear();
  //console.log("start: "+start_date+"end: "+end_date);

var url = "https://www.nlrb.gov/news-outreach/graphs-data/recent-filings/export/cvs?f[0]=date%3A"+start_date+"%20to%20"+end_date+"&attach=page"
  console.log(url)
  request(url, function (error, response, body) {
    if (!error) {
      //console.log(response);
      console.log("body")
      csv.parse(body, function(err, data){

          //console.log(data);
          connection.query('INSERT INTO nlrb_dl_node (Name,`Case Number`,City,State,`Date Filed`, `Region Assigned`,Status,`Date Closed`,`Reason Closed`,`No. of Eligible Voters`,`No. of Employees`,`Certified Representative`,`Unit Sought`) VALUES ?', [data], function(err, result) {
            if(err)throw err;
              console.log("bulk insert");
              next();

          });
      });
    } else {
      console.log("We’ve encountered an error: " + error);
    }
  });
  unix_timestamp = unix_timestamp+604800;
},
function (err) {
  // All things are done!
  console.log(err)
});
